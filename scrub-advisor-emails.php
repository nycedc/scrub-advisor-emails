<?php

function scrub_email(string $email): string
{
    [$name] = explode('@', $email);

    return "$name@example.com";
}

function scrub_advisor_emails()
{
    global $wpdb;

    // Change all Booknetic Advisor WP Users to have an email with "example.com"
    $advisors = get_users([
        'role' => 'booknetic_staff',
    ]);

    foreach ($advisors as $advisor) {
        wp_update_user([
            'ID' => $advisor->ID,
            'user_email' => scrub_email($advisor->get('user_email')),
        ]);
    }

    // Change all Booknetic Staff to have an email with "example.com"
    $staff = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'bkntc_staff', ARRAY_A);

    foreach ($staff as $member) {
        $email = $member['email'];
        $id = $member['id'];
        $wpdb->update(
            $wpdb->prefix . 'bkntc_staff',
            [
                'email' => scrub_email($email),
                'zoom_user' => '{"id":"","name":""}',
            ],
            // Unset all Booknetic Staff Zoom Users
            ['id' => $id],
        );
    }

    // Scrub all Booknetic Customer emails
    $customers = $wpdb->get_results('SELECT * FROM '. $wpdb->prefix . 'bkntc_customers', ARRAY_A);

    foreach ($customers as $c) {
        $email = $c['email'];
        $id = $c['id'];
        $wpdb->update(
            $wpdb->prefix . 'bkntc_customers',
            ['email' => scrub_email($email)],
            ['id' => $id],
        );
    }

}

scrub_advisor_emails();
