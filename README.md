# Scrub Advisor Emails

## Description
This is meant as a one off script to ensure staging sites don't contain actual advisor email information.

## Usage
Meant to be run as a one-off script with wp-cli's [eval-file](https://developer.wordpress.org/cli/commands/eval-file/) command.
